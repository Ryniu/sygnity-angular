import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./playlists/views/playlists-view/playlists-view.component";
import { MusicSearchComponent } from "./music-search/views/music-search/music-search.component";
// http://localhost:4200/#/playlists

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "playlists",
    component: PlaylistsViewComponent
  },
  {
    path: "playlists/:playlist_id",
    component: PlaylistsViewComponent
  },
  {
    path: "search",
    component: MusicSearchComponent
  },
  {
    path: "**",
    // component: PageNotFoundComponent
    redirectTo: "playlists",
    pathMatch: "prefix"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
