import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album } from "src/app/models/album";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";
import { AlbumsResponse } from "../../models/album";
import {
  map,
  pluck,
  catchError,
  skip,
  tap,
  switchMap,
  startWith
} from "rxjs/operators";
import {
  EMPTY,
  NEVER,
  throwError,
  concat,
  of,
  Subject,
  ReplaySubject,
  BehaviorSubject
} from "rxjs";

export const API_URL = new InjectionToken("Api url for music search"); //

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  search(query: string = "batman") {
    this.http
      .get<AlbumsResponse>(this.api_url, {
        params: {
          q: query,
          type: "album"
        }
      })
      .pipe(map(resp => resp.albums.items))
      .subscribe({
        next: albums => this.albumChanges.next(albums),
        error: error => this.errorNotifications.next(error.message)
      });
  }

  constructor(
    @Inject(API_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    (window as any).subject = this.albumChanges
  }

  results: Album[] = [
    {
      id: "123",
      name: "Placki23423",
      artists: [],
      images: [
        {
          url: "https://www.placecage.com/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Plack123i",
      artists: [],
      images: [
        {
          url: "https://www.placecage.com/400/400"
        }
      ]
    },
    {
      id: "123",
      name: "Placki",
      artists: [],
      images: [
        {
          url: "https://www.placecage.com/300/300"
        }
      ]
    }
  ];

  errorNotifications = new Subject<Error>();
  albumChanges = new BehaviorSubject<Album[]>(this.results);

  getAlbums() {
    return this.albumChanges.asObservable();
  }
}
