import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchComponent } from "./views/music-search/music-search.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import {
  API_URL /* , MusicSearchService */
} from "./services/music-search.service";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule, 
    ReactiveFormsModule, 
    MusicSearchRoutingModule
  ],
  exports: [MusicSearchComponent],
  providers: [
    {
      provide: API_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory: (url: string) => {
    //     return new MusicSearchService(url);
    //   },
    //   deps: [API_URL]
    // },
    // {
    //   provide: MusicSearchService,
    //   useClass: MusicSearchService,
    //   // deps: [API_URL]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService
    // },
    // MusicSearchService,
  ]
})
export class MusicSearchModule {}
