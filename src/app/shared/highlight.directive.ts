import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  OnDestroy,
  DoCheck,
  SimpleChanges,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]",
  exportAs:'Placki',
  // host: {
  //   "(mouseenter)": "activate($event) ",
  //   "(mouseleave)": " deactivate($event) ",
  //   "[style.border-left-color]": "activeColor "
  // }
})
export class HighlightDirective
  implements OnChanges, OnInit, OnDestroy, DoCheck {
  
  @Input("appHighlight")
  color = "red";

  // get fun(){
  //   return this.active? this.color : ''
  // }

  @HostBinding("style.border-left-color")
  activeColor = "";

  constructor(
    private elem: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
    // console.log("Hello Higlight", this.color);
  }

  @HostBinding('class.placki')
  active = false;

  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x: number, y: number) {
    // this.activeColor = this.color;
    this.active = true;
  }

  @HostListener("mouseleave", [])
  deactivate() {
    // this.activeColor = "";
    this.active = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    // this.elem.nativeElement.style.color = this.color;
    // this.renderer.listen(this.elem.nativeElement,'mouseenter',console.log)
    // this.renderer.setStyle(this.elem.nativeElement,'color',this.color)

    // console.log("ngOnChanges", changes);
  }

  ngOnInit(): void {
    // console.log("ngOnInit");
  }
  
  ngOnDestroy(): void {
    // console.log("ngOnDestroy");
  }

  ngDoCheck(): void {
    this.activeColor = this.active ? this.color : "";
    // console.log("ngDoCheck");
  }
}

// console.log(HighlightDirective)