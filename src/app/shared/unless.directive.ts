import { PlaylistsViewComponent } from "../playlists/views/playlists-view/playlists-view.component";
import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  ComponentFactoryResolver
} from "@angular/core";

interface Context {}

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  @Input("appUnless")
  hide = false;

  count = 0;

  constructor(
    private tpl: TemplateRef<Context>,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {}

  ngOnChanges() {
    if (this.hide) {
      this.vcr.clear();
    } else {
      // this.vcr.createEmbeddedView(
      //   this.tpl,
      //   {
      //     message: "placki ",
      //     $implicit: this.count++
      //   },
      //   this.vcr.length
      // );

      const f = this.cfr.resolveComponentFactory(PlaylistsViewComponent);

      const cRef = this.vcr.createComponent(f, 0, undefined, []);

      cRef.instance.selected = cRef.instance.playlists[2];
    }
  }
}
