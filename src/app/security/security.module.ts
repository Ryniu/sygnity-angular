import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthService, AuthConfig } from "./auth.service";
import { environment } from "../../environments/environment";
import { ModuleWithProviders } from "@angular/compiler/src/core";
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS
} from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.authConfig
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ]
})
export class SecurityModule {
  constructor(private auth: AuthService) {
    this.auth.getToken();
  }

  static forRoot(config: AuthConfig): ModuleWithProviders {
    return {
      ngModule: SecurityModule,
      providers: [
        {
          provide: AuthConfig,
          useValue: config
        }
      ]
    };
  }
}
