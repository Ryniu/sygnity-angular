import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  AfterViewInit,
  AfterViewChecked,
  AfterContentInit,
  AfterContentChecked
} from "@angular/core";
import { Playlist } from "../../../models/playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent
  implements
    OnInit,
    AfterViewInit,
    AfterViewChecked,
    AfterContentInit,
    AfterContentChecked {
  @ViewChild("formRef", { static: true, read: NgForm })
  formRef?: NgForm;

  @Input()
  playlist!: Playlist;

  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter<Playlist>();

  constructor() {}

  saveDraft(formRef: NgForm) {
    const draft = {
      ...this.playlist,
      ...formRef.value
    };

    this.save.emit(draft);
  }

  ngOnInit(): void {
    // console.log("ngOnInit");
  }

  ngAfterContentInit(): void {
    // console.log("ngAfterContentInit");
  }

  ngAfterViewInit() {
    // console.log("ngAfterViewInit");
    setTimeout(() => {
      // console.log(this.formRef?.controls.name);
    });
  }

  ngAfterContentChecked(): void {
    // console.log("ngAfterContentChecked");
  }

  ngAfterViewChecked(): void {
    // console.log("ngAfterViewChecked");
  }
}
