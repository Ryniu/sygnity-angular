import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "../../../models/playlist";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist!: Playlist;

  constructor() {}

  @Output() edit = new EventEmitter();

  ngOnInit(): void {}
}
