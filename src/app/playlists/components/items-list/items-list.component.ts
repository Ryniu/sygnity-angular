import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from '../../../models/playlist';
import { NgForOf, NgForOfContext } from "@angular/common";

NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"]
  // inputs: ["playlists:items"]
})
export class ItemsListComponent {
  @Input("items")
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist | null = null;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(selected: Playlist) {
    this.selectedChange.emit(selected);

    // this.selected = selected == this.selected ? null : selected;
  }

  constructor() {}

  ngOnInit(): void {}

  trackFn(i:number,item:Playlist){
    return item.id
  }
}
